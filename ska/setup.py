#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='tangogql',
    version="0.2.10",
    description="",
    long_description=readme + '\n\n',
    author="Stewart Williams",
    author_email='stewart.williams@stfc.ac.uk',
    url='https://gitlab.com/ska-telescope/web-maxiv-tangogql',
    packages=[
        'tangogql',
    ],
    package_dir={'tangogql': 'tangogql'},
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    keywords='ska_tango_graphql',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=['pytango'],
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
        'recommonmark'
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-json-report',
        'pycodestyle',
    ],
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort']
    }
)
