# Tango GraphQL

A GraphQL interface for Tango.

## Description

This is an attempt at using "modern" web standards to make a TANGO web service.</br>
It provides websocket communication for subscribing to attributes, and a GraphQL interface to the TANGO database.

## Usage

The server is written in Python and currently requires python 3.6 or later.

__aiohttp__ is used for the web server part, (graphene)[http://graphene-python.org/] for the GraphQL part.</br>
"requirements.txt" should list the necessary libraries, which can be installed using "pip".</br>
Also, a Conda environment can be created using the *_environment.yml*_.</br>

If preferred, a Dockerfile is provided and can be used to run the server.</br>

If the intention is to run it manually, once all the dependencies are installed, you can start the server by doing:

```shell
$ python -m tangogql
```

If you want to run the server in a read only mode, where the access to the control system is done in a read only way, you can use the environment variable: READ_ONLY, and set it to 1. If the jwt authentication is not
set up then the server can be run without authentication by setting the environment variable NO_AUTH to 1.

The requests are made to the url: http://localhost:5004/db

## Installation

At the moment of writing this, there is no packaging system ready, making the best deployment option the usage of the Docker Container.

### For development

1. `cd` into the `web-maxiv-tangoql` directory and run `docker-compose up`.
2. Wait
3. Open your browser to [localhost:5004/graphiql](http://localhost:5004/graphiql) to verify that it works.

A tool called [aiohttp-devtools](https://github.com/aio-libs/aiohttp-devtools) is used to auto-reload the server inside the Docker container whenever the code changes.

The docker-compose.yml file actually overwrites the start script in order to run the container with the aiohttp-devtools instead of a normal startup. This should only be used for development.

## License

TangoGQL is released under the license that can be found in the LICENCE file in the root directory of the project.

## Authors

Tango GraphQL was written by the KITS Group at MAX IV Laboratory.

Special thanks to:

- Johan Forsberg and Vincent Michel
- Linh Nguyen
