#!/usr/bin/env python3
# pylint: disable=R0201
# pylint: disable=C0116

"""Functional tests for the schema."""

import asyncio
import pytest

from graphene.test import Client
from graphql.execution.executors.asyncio import AsyncioExecutor

from tests.unit import queries
from unittest.mock import MagicMock

# The test doubles for the Tango database and the Device Proxy Cache
# that will return the test double for the Device Proxy.
from tests.unit.test_schema_mocks import MockDB, MockDeviceProxyCache

# I can't figure out a better way of doing this. The issue is that the original import has a
# side-effect the moment query or base or database are imported it will try to create a
# connection to a Tango DB which is what we are trying to avoid.

import sys

mock_base = MagicMock()
sys.modules['tangogql.schema.base'] = mock_base
mock_base.db = MockDB()
mock_base.proxies = MockDeviceProxyCache()

# and so this import has to be here - after base has been patched to reference the mock objects
# and after that we need to define a new TanngogqlClient and client fixture based on it  to
# ensure it is this version of tangoschema that is used.
from tangogql.schema.tango import tangoschema


class TangogqlClient(object):
    """Simulated  TangoGQL client used for unit tests"""

    def __init__(self):
        self.client = Client(tangoschema, allow_subscriptions=True)

    def execute(self, query):
        """ Run the provided query within the event loop"""
        loop = asyncio.get_event_loop()
        r = self.client.execute(query, executor=AsyncioExecutor(loop=loop))
        return r["data"]


@pytest.fixture
def client():
    """
    Client fixture used by unit tests to send queries to the TangoGQL code
     - simulating requests from WebJive
     """
    client = TangogqlClient()
    return client


@pytest.mark.usefixtures("client")
class TestDeviceClass:
    """Tests primarily of the ability to query the properties of the tango device"""

    def test_device_resolve_name(self, client):
        result = client.execute(queries.DEVICE_NAME_QUERY)
        assert 'devices' in result
        assert "name" in result['devices'][0]
        assert result['devices'][0]['name'] == "sys/tg_test/1"

    def test_device_resolve_single_name(self, client):
        result = client.execute(queries.SINGLE_DEVICE_NAME_QUERY)
        assert 'device' in result
        assert "name" in result['device']
        assert result['device']['name'] == "sys/tg_test/1"

    def test_device_resolve_state(self, client):
        result = client.execute(queries.DEVICE_STATE_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['state'], str)
        assert len(result['state']) > 0

    def test_device_resolve_properties(self, client):
        result = client.execute(queries.DEVICE_PROPERTIES_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['properties'], list)
        result = result['properties'][0]
        assert isinstance(result, dict)
        assert "name" in result
        assert "device" in result
        assert "value" in result

    def test_device_resolve_attributes(self, client):
        result = client.execute(queries.DEVICE_ATTRIBUTES_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['attributes'], list)
        result = result['attributes'][0]
        assert isinstance(result, dict)
        assert "name" in result
        assert "device" in result
        assert "datatype" in result
        assert "format" in result
        assert "dataformat" in result
        assert "writable" in result
        assert "label" in result
        assert "unit" in result
        assert "description" in result
        assert "displevel" in result
        assert "value" in result
        assert "quality" in result
        assert "minvalue" in result
        assert "maxvalue" in result
        assert "minalarm" in result
        assert "maxalarm" in result
        assert "timestamp" in result
        assert "enumLabels" in result
        assert result['name'].islower()
        for key, value in result.items():
            if key in ['minvalue', 'maxvalue', 'minalarm', 'maxalarm', 'timestamp', 'enumLabels']:
                if value is None:
                    assert value is None
                else:
                    assert isinstance(value, (int, float, str, list))
            elif key not in ['value', 'writevalue']:
                assert isinstance(value, str)
            else:
                assert isinstance(value, (int, float)), "value  is not a number: %s" % key

    def test_lower_cases(self, client):
        #check if the attribute name is tranformed in lower case (ampli) also if in the
        #test_schema_mocks is written witn a capital letter (Ampli)
        result = client.execute(queries.DEVICE_ATTRIBUTES_QUERY)
        result = result['devices'][0]
        result = result['attributes'][0]
        for key, value in result.items():
            if key == 'name':
                assert value.islower()

    def test_device_resolve_commands(self, client):
        result = client.execute(queries.DEVICE_COMMANDS_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['commands'], list)
        result = result['commands'][0]
        assert isinstance(result, dict)
        assert "name" in result
        assert "tag" in result
        assert "displevel" in result
        assert "intype" in result
        assert "intypedesc" in result
        assert "outtype" in result
        assert "outtypedesc" in result
        for key, value in result.items():
            if key != 'tag':
                assert isinstance(value, str)
            else:
                assert isinstance(value, (int, float))

    def test_device_resolve_server(self, client):
        result = client.execute(queries.DEVICE_SERVER_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['server'], dict)
        result = result['server']
        assert "id" in result
        assert "host" in result
        for _, value in result.items():
            assert isinstance(value, str)

    def test_device_resolve_class(self, client):
        result = client.execute(queries.DEVICE_CLASS_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['deviceClass'], str)

    def test_device_resolve_pid(self, client):
        result = client.execute(queries.DEVICE_PID_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['pid'], int)

    def test_device_resolve_started_date(self, client):
        result = client.execute(queries.DEVICE_STARTED_DATE_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['startedDate'], str)

    def test_device_resolve_stopped_date(self, client):
        result = client.execute(queries.DEVICE_STOPPED_DATE_QUERY)
        assert 'devices' in result
        result = result['devices'][0]
        assert isinstance(result['stoppedDate'], str)


@pytest.mark.usefixtures("client")
class TestDomainClass:
    """Tests primarily of the ability to resolve domain properties"""

    def test_domain_resolve_name(self, client):
        result = client.execute(queries.DOMAIN_NAME_QUERY)
        assert 'domains' in result
        assert "name" in result['domains'][0]
        result = result['domains'][0]
        for _, value in result.items():
            assert isinstance(value, str)

    def test_domain_resolve_families(self, client):
        result = client.execute(queries.domain_families)
        assert 'domains' in result
        result = result['domains'][0]
        assert "families" in result
        result = result['families'][0]
        assert "name" in result
        assert "domain" in result
        assert "members" in result

        assert isinstance(result['name'], str)
        assert isinstance(result['domain'], str)

        result = result['members'][0]
        assert "name" in result and isinstance(result['name'], str)
        assert "state" in result and isinstance(result['state'], str)
        assert "pid" in result and isinstance(result['pid'], int)
        assert "startedDate" in result and isinstance(result['startedDate'], str)
        assert "stoppedDate" in result and isinstance(result['stoppedDate'], str)
        assert "exported" in result and isinstance(result['exported'], bool)
        assert "domain" in result and isinstance(result['domain'], str)
        assert "family" in result and isinstance(result['family'], str)


class TestMemberClass:
    """Tests primarily of the ability to retrieve member details"""

    def test_member_resolve_name(self, client):
        result = client.execute(queries.member_name)
        assert 'members' in result
        result = result['members'][0]
        assert 'name' in result
        assert isinstance(result['name'], str)

    def test_member_resolve_state(self, client):
        result = client.execute(queries.member_state)
        assert 'members' in result
        result = result['members'][0]
        assert 'state' in result
        assert isinstance(result['state'], str)

    def test_member_resolve_device_class(self, client):
        result = client.execute(queries.member_device_class)
        assert 'members' in result
        result = result['members'][0]
        assert 'deviceClass' in result
        assert isinstance(result['deviceClass'], str)

    def test_member_resolve_pid(self, client):
        result = client.execute(queries.member_pid)
        assert 'members' in result
        result = result['members'][0]
        assert 'pid' in result
        assert isinstance(result['pid'], int)

    def test_member_resolve_started_date(self, client):
        result = client.execute(queries.member_startedDate)
        assert 'members' in result
        result = result['members'][0]
        assert 'startedDate' in result
        assert isinstance(result['startedDate'], str)

    def test_member_resolve_stopped_date(self, client):
        result = client.execute(queries.member_stoppedDate)
        assert 'members' in result
        result = result['members'][0]
        assert 'stoppedDate' in result
        assert isinstance(result['stoppedDate'], str)

    def test_member_resolve_exported(self, client):
        result = client.execute(queries.member_exported)
        assert 'members' in result
        result = result['members'][0]
        assert 'exported' in result
        assert isinstance(result['exported'], bool)

    def test_member_resolve_domain(self, client):
        result = client.execute(queries.member_domain)
        assert 'members' in result
        result = result['members'][0]
        assert 'domain' in result
        assert isinstance(result['domain'], str)

    def test_member_resolve_family(self, client):
        result = client.execute(queries.member_family)
        assert 'members' in result
        result = result['members'][0]
        assert 'family' in result
        assert isinstance(result['family'], str)

# Test of mutation classes

# class TestPutDevicePropertyClass(object):
#
#     def test_put_device_property_mutate(self, client):
#         result = client.execute(queries.putDeviceProperty)
#         assert "putDeviceProperty" in result
#         result = result['putDeviceProperty']
#         assert "ok" in result and isinstance(result['ok'], bool)
#         assert "message" in result and isinstance(result['message'], list)
#         for m in result['message']:
#             assert (isinstance(m, str))
#
#
# class TestDeleteDevicePropertyClass(object):
#
#     def test_delete_device_property_mutate(self, client):
#         result = client.execute(queries.deleteDeviceProperty)
#         assert "deleteDeviceProperty" in result
#         result = result['deleteDeviceProperty']
#         assert "ok" in result and isinstance(result['ok'], bool)
#         assert "message" in result and isinstance(result['message'], list)
#         for m in result['message']:
#             assert (isinstance(m, str))


# @pytest.mark.usefixtures("client")
# class TestExecuteDeviceCommandClass(object):

# def test_execute_device_command_mutate(self, client):
#     result = client.execute(queries.executeDeviceCommand)
#     assert "executeCommand" in result
#     result = result['executeCommand']
#     assert "ok" in result and isinstance(result['ok'], bool)
#     assert result['ok']
#     assert "output" in result and isinstance(result['output'],
#                                              (str, bool, int, float))
#     assert "message" in result and isinstance(result['message'], list)
#     assert result["message"][0] == "Success"
#
# def test_execute_device_command_mutate_wrong_input_type(self, client):
#     result = client.execute(queries.executeDeviceCommand_wrong_input_type)
#     assert "executeCommand" in result
#     result = result['executeCommand']
#     assert "ok" in result and isinstance(result['ok'], bool)
#     assert result['ok'] is False
#     assert "message" in result and isinstance(result['message'], list)
#     for m in result['message']:
#         assert isinstance(m, str)
#
#     msg = "The input value is not of acceptable types"
#     assert result["message"][0] == msg
#     assert result["output"] is None
#
# def test_execute_device_command_mutate_none_exist_command(self, client):
#     result = client.execute(
#         queries.executeDeviceCommand_none_exist_command
#     )
#     assert "executeCommand" in result
#     result = result['executeCommand']
#     assert "ok" in result and isinstance(result['ok'], bool)
#     assert result['ok'] is False
#     assert result["output"] is None
#     assert "message" in result and isinstance(result['message'], list)
#     for m in result['message']:
#         assert isinstance(m, str)
#     result = result["message"]
#     assert result[0] == "Command dfg not found"
#     assert result[1] == "API_CommandNotFound"


# @pytest.mark.usefixtures("client")
# class TestSetAttributeValueClass(object):
#
#     def test_set_attribute_value_mutate_none_exist_attr(self, client):
#         result = client.execute(queries.setAttributeValue_none_exist_attr)
#         assert "setAttributeValue" in result
#         result = result['setAttributeValue']
#         assert "ok" in result and isinstance(result['ok'], bool)
#         assert result['ok'] is False
#         assert "message" in result and isinstance(result['message'], list)
#         for m in result['message']:
#             assert isinstance(m, str)
#
#     def test_set_attribute_value_mutate(self, client):
#         result = client.execute(queries.setAttributeValue)
#         assert "setAttributeValue" in result
#         result = result['setAttributeValue']
#         assert "ok" in result and isinstance(result['ok'], bool)
#         assert result['ok']
#         assert "message" in result and isinstance(result['message'], list)
#         for m in result['message']:
#             assert isinstance(m, str)
#         assert result["message"][0] == "Success"
#
#     def test_set_attribute_value_mutate_wrong_input_type(self, client):
#         result = client.execute(queries.setAttributeValue_wrong_input_type)
#         assert "setAttributeValue" in result
#         result = result['setAttributeValue']
#         assert "ok" in result and isinstance(result['ok'], bool)
#         assert result['ok'] is False
#         assert "message" in result and isinstance(result['message'], list)
#         for m in result['message']:
#             assert (isinstance(m, str))
