#!/usr/bin/env python3

import asyncio
import pytest

from graphene.test import Client
from graphql.execution.executors.asyncio import AsyncioExecutor

"""Configuration and commons for tests."""
__author__ = "antmil"
__docformat__ = "restructuredtext"

class TangogqlClient(object):
    """Simulated  TangoGQL client used for unit tests"""
    def __init__(self):
        self.client = Client(tangoschema)

    def execute(self, query):
        loop = asyncio.get_event_loop()
        r = self.client.execute(query, executor=AsyncioExecutor(loop=loop))
        return r["data"]


@pytest.fixture(autouse=True)
def run_around_tests():
    """
    Create a new event loop for every test. This stops scheduled jobs from
    previous tasks from leaking into the subsequent test.
    """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    yield
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()


@pytest.fixture
def client():
    """ Client fixture used by unit tests to send queries to the TangoGQL code - simulating requests from WebJive"""
    client = TangogqlClient()
    return client
